/*
 * Copyright 2016-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * To start type 'npm start' or 'node ./app.js'
 */

/* jshint esversion: 6, node: true, devel: true */
'use strict';

const 
  bodyParser = require('body-parser'),
  config = require('config'),
  crypto = require('crypto'),
  express = require('express'),
  https = require('https'),  
  request = require('request'),
  apiai = require('apiai'),
  messages = require("./lang/messages");
  var superagent = require('superagent');



/*
 * Be sure to setup your config values before running this code. You can 
 * set them using environment variables or modifying the config file in /config.
 */

// App Secret can be retrieved from the App Dashboard
const APP_SECRET = (process.env.MESSENGER_APP_SECRET) ? 
  process.env.MESSENGER_APP_SECRET :
  config.get('Facebook.appSecret');

// Arbitrary value used to validate a webhook
const VALIDATION_TOKEN = (process.env.MESSENGER_VALIDATION_TOKEN) ?
  (process.env.MESSENGER_VALIDATION_TOKEN) :
  config.get('Facebook.validationToken');

// Generate a page access token for your page from the App Dashboard
const PAGE_ACCESS_TOKEN = (process.env.MESSENGER_PAGE_ACCESS_TOKEN) ?
  (process.env.MESSENGER_PAGE_ACCESS_TOKEN) :
  config.get('Facebook.pageAccessToken');

// Api.Ai access token
const APIAI_TOKEN = (process.env.APIAI_TOKEN) ? 
  (process.env.APIAI_TOKEN) : 
  config.get('ApiAi.token');

if (!(APP_SECRET && VALIDATION_TOKEN && PAGE_ACCESS_TOKEN && APIAI_TOKEN)) {
  console.error("Missing config values");
  process.exit(1);
}

// Create the Api.Ai Object
const api = apiai(APIAI_TOKEN);
//const apikey = 4491e9c83894df190f1aa98c3866c3134fd2639e;

var mailbox = require('helpscout')('4491e9c83894df190f1aa98c3866c3134fd2639e', 76268);

// Execute custom actions when receiving a completed action from Api.Ai
const actions = {
  ['greeting'] : (recipientId, messageText, response) => {

    sendTemplateMessage(recipientId, {
        template_type: "button",
        text: messageText,
        buttons:[{
          type: "postback",
          title: messages.BUTTON_SOMETHINGISWRONG,
          payload: messages.ISSUE_SOMETHINGISWRONG
        }, {
          type: "postback",
          title: messages.BUTTON_SOMETHINGELSE,
          payload: messages.ISSUE_SOMETHINGELSE
        }, {
          type: "postback",
          title: messages.BUTTON_EVERYTHINGISAWESOME,
          payload: messages.ISSUE_EVERYTHINGISAWESOME
        }]
      }
    );
  },

  ['something_is_wrong_action'] : (recipientId, messageText, response) => {
        console.log('trying to run something is wrong');
        sendTemplateMessage(recipientId, {
        template_type: "button", 
         text: messageText,
         buttons:[{
          type: "postback",
          title: messages.BUTTON_EARNINGMOREPOINTS,
          payload: messages.ISSUE_EARNINGMOREPOINTS,
        }, {
          type: "postback",
          title: messages.BUTTON_MOREOPTIONSTOREDEEM,
          payload: messages.ISSUE_MOREOPTIONSTOREDEEM
        }, {
          type: "postback",
          title: messages.BUTTON_SOMETHINGISBROKEN,
          payload: messages.ISSUE_SOMETHINGISBROKEN
        }]
      }
    );
  },



['issue-create-support-ticket'] : (recipientId, messageText, response) => {

	//response.results.parameter ..log it 
		if (!response.result.actionIncomplete){

			var email = response.result.parameters['email'];
			var subject = response.result.parameters['subject-line'];
			var body = response.result.parameters['email-body'];

			console.log(email);
			console.log(subject);
			console.log(body);

			var conversation = {
				"customer": {
					"email": email
				},
				"subject": subject,
				"mailbox": {
					"id": 76268
				},

				 "threads": [{
            "type": "customer",
            "createdBy": {
    
                "email": email,
                "type": "customer"
            },
            "body": body,
        }
         
   		 ],};

			superagent
   				.post('https://api.helpscout.net/v1/conversations.json')
    			.auth('4491e9c83894df190f1aa98c3866c3134fd2639e', 'X')
    			.send(conversation)
    			.end(function(err, res) {
      			if(!err){
					sendTextMessage(recipientId, messageText);
					console.log(res);
				}else{
					//console.log(err);
				}
   			 });	
		}else{

			sendTextMessage(recipientId, messageText);
		}
		
	},

  ['something-else-action'] : (recipientId, messageText, response) => {
        sendTemplateMessage(recipientId, {
        template_type: "button", 
         text: messageText,
         buttons:[{
          type: "postback",
          title: messages.BUTTON_POINTSWORTH,
          payload: messages.ISSUE_POINTSWORTH,
        }, {
          type: "postback",
          title: messages.BUTTON_WHENCANICASHOUT,
          payload: messages.ISSUE_WHENCANICASHOUT
        }, {
          type: "postback",
          title: messages.BUTTON_HOWTOEARNPOINTS,
          payload: messages.ISSUE_HOWTOEARNPOINTS
        }]
      }
    );
  },


 ['ad-issue-action'] : (recipientId, messageText, response) => {
        sendTemplateMessage(recipientId, {
        template_type: "button", 
         text: messageText,
         buttons:[{
          type: "postback",
          title: messages.BUTTON_ADSNOTLOADING,
          payload: messages.ISSUE_ADSNOTLOADING,
        }, {
          type: "postback",
          title: messages.BUTTON_SAMEADS,
          payload: messages.ISSUE_SAMEADS
         }, {
          type: "postback",
          title: messages.BUTTON_DONTLIKETHEADS,
          payload: messages.ISSUE_DONTLIKETHEADS
        }]
      }
    );
  },



['everything-is-amazing'] : (recipientId, messageText, response) => {
        sendTemplateMessage(recipientId, {
        template_type: "button", 
         text: messageText,
         buttons:[{
          type: "web_url",
          title: messages.BUTTON_FEATUREREQUEST,
          url: messages.ISSUE_FEATUREREQUEST
        }, {
          type: "web_url",
          title: messages.BUTTON_REVIEW,
          url: messages.ISSUE_REVIEW
         }, {
          type: "web_url",
          title: messages.BUTTON_LEARNMORE,
          url: messages.ISSUE_LEARNMORE
        }]
      }
    );
  },


['want-more-points'] : (recipientId, messageText, response) => {
        sendTemplateMessage(recipientId, {
        template_type: "button", 
         text: messageText,
         buttons:[{
          type: "web_url",
          title: messages.BUTTON_FAQS,
          url: messages.ISSUE_FAQS
        }] 
      }
    );
  },

['more-ways-redeem'] : (recipientId, messageText, response) => {
        sendTemplateMessage(recipientId, {
        template_type: "button", 
         text: messageText,
         buttons:[{
          type: "web_url",
          title: messages.BUTTON_REDEEM_WAYS,
          url: messages.ISSUE_REDEEM_WAYS
        }] 
      }
    );
  },


  ['something-is-broken-action'] : (recipientId, messageText, response) => {
        sendTemplateMessage(recipientId, {
        template_type: "button", 
         text: messageText,
         buttons:[{
          type: "postback",
          title: messages.BUTTON_POINTSARESTUCK,
          payload: messages.ISSUE_POINTSARESTUCK
        }, {
          type: "postback",
          title: messages.BUTTON_MYPOINTSDISAPPEARED,
          payload: messages.ISSUE_MYPOINTSDISAPPEARED
        }, {
          type: "postback",
          title: messages.BUTTON_ISSUEWITHADS,
          payload: messages.ISSUE_ISSUEWITHADS
        }]
      }
    );
  }
}

// This will contain all user sessions.
// Each session has an entry:
// sessionId -> {fbid: facebookUserId, context: sessionState}
const sessions = {};

const findOrCreateSession = (fbid) => {
  let sessionId;
  // Let's see if we already have a session for the user fbid
  Object.keys(sessions).forEach(k => {
    if (sessions[k].fbid === fbid) {
      // Yep, got it!
      sessionId = k;
    }
  });
  if (!sessionId) {
    // No session found for user fbid, let's create a new one
    sessionId = new Date().toISOString();
    sessions[sessionId] = {fbid: fbid, context: {}};
  }
  return sessionId;
};

/*
 * Verify that the callback came from Facebook. Using the App Secret from 
 * the App Dashboard, we can verify the signature that is sent with each 
 * callback in the x-hub-signature field, located in the header.
 *
 * https://developers.facebook.com/docs/graph-api/webhooks#setup
 *
 */
const verifyRequestSignature = (req, res, buf) => {
  var signature = req.headers["x-hub-signature"];

  if (!signature) {
    // For testing, let's log an error. In production, you should throw an 
    // error.
    console.error("Couldn't validate the signature.");
  } else {
    var elements = signature.split('=');
    var method = elements[0];
    var signatureHash = elements[1];

    var expectedHash = crypto.createHmac('sha1', APP_SECRET).update(buf).digest('hex');

    if (signatureHash != expectedHash) {
      throw new Error("Couldn't validate the request signature.");
    }
  }
};

/*
 * Authorization Event
 *
 * The value for 'optin.ref' is defined in the entry point. For the "Send to 
 * Messenger" plugin, it is the 'data-ref' field. Read more at 
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference#auth
 *
 */
const receivedAuthentication = (event) => {
  var senderId = event.sender.id;
  var recipientId = event.recipient.id;
  var timeOfAuth = event.timestamp;

  // The 'ref' field is set in the 'Send to Messenger' plugin, in the 'data-ref'
  // The developer can set this to an arbitrary value to associate the 
  // authentication callback with the 'Send to Messenger' click event. This is
  // a way to do account linking when the user clicks the 'Send to Messenger' 
  // plugin.
  var passThroughParam = event.optin.ref;

  console.log("Received authentication for user %d and page %d with pass " +
    "through param '%s' at %d", senderId, recipientId, passThroughParam, 
    timeOfAuth);

  // When an authentication is received, we'll send a message back to the sender
  // to let them know it was successful.
  sendTextMessage(senderId, "Authentication successful");
};

/*
 * Message Event
 *
 * This event is called when a message is sent to your page. The 'message' 
 * object format can vary depending on the kind of message that was received.
 * Read more at https://developers.facebook.com/docs/messenger-platform/webhook-reference#received_message
 *
 * For this example, we're going to echo any text that we get. If we get some 
 * special keywords ('button', 'generic', 'receipt'), then we'll send back
 * examples of those bubbles to illustrate the special message bubbles we've 
 * created. If we receive a message with an attachment (image, video, audio), 
 * then we'll simply confirm that we've received the attachment.
 * 
 */
const receivedMessage = (event) => {
  var senderId = event.sender.id;
  var recipientId = event.recipient.id;
  var timeOfMessage = event.timestamp;
  var message = event.message;

  console.log("Received message for user %d and page %d at %d with message:", senderId, recipientId, timeOfMessage);
  console.log(JSON.stringify(message));

  var messageId = message.mid;

  // You may get a text or attachment but not both
  var messageText = message.text;
  var messageAttachments = message.attachments;

  if (messageText) {

    // We retrieve the user's current session, or create one if it doesn't exist
    // This is needed for our bot to figure out the conversation history
    const sessionId = findOrCreateSession(senderId);

    // Process the message text
    processTextMessage(sessionId, messageText);
  } else if (messageAttachments) {
    sendTextMessage(senderId, "Sorry but we currently cannot process messages with attachements.");
  }
};

/*
 * Api.Ai Response Processing Event
 * 
 * Process the Api.Ai response that we get after processing a message
 */
const processResponse = (response) => {
  const sessionId = response.sessionId;

  console.log(JSON.stringify(response));

  // Our bot has something to say!
  // Let's retrieve the Facebook user whose session belongs to
  const recipientId = sessions[sessionId].fbid;
  const result = response.result;
  if (recipientId) {
    // Yay, we found our recipient!
    // Let's forward our bot response to her.
    if(result && result.fulfillment) {
      // Search for our custom action and run it if it exists
      const actionName = response.result.action;
      if(actionName in actions) {
        actions[actionName](recipientId, result.fulfillment.speech, response);
      } else {
        sendTextMessage(recipientId, result.fulfillment.speech);
      }
    }
  } else {
    console.log('Oops! Couldn\'t find user for session:', sessionId);
  }
};

/*
 * Api.Ai Message Processing Event
 *
 * Send the message to Api.Ai for processing of the user's intent
 */
const processTextMessage = (sessionId, messageText) => {
  // Let's forward the message to the Api.Ai Bot Engine
  const request = api.textRequest(messageText, {
		sessionId : sessionId 
	});

	request.on('response', function(response) {
			processResponse(response);
	});

	request.on('error', function(error) {
			console.log('Oops! Got an error from Api.Ai:', error);
	});

	request.end();
};

/*
 * Delivery Confirmation Event
 *
 * This event is sent to confirm the delivery of a message. Read more about 
 * these fields at https://developers.facebook.com/docs/messenger-platform/webhook-reference#message_delivery
 *
 */
const receivedDeliveryConfirmation = (event) => {
  var senderId = event.sender.id;
  var recipientId = event.recipient.id;
  var delivery = event.delivery;
  var messageIDs = delivery.mids;
  var watermark = delivery.watermark;
  var sequenceNumber = delivery.seq;

  if (messageIDs) {
    messageIDs.forEach(function(messageID) {
      console.log("Received delivery confirmation for message ID: %s", messageID);
    });
  }

  console.log("All message before %d were delivered.", watermark);
};

/*
 * Postback Event
 *
 * This event is called when a postback is tapped on a Structured Message. Read
 * more at https://developers.facebook.com/docs/messenger-platform/webhook-reference#postback
 * 
 */
const receivedPostback = (event) => {
  var senderId = event.sender.id;
  var recipientId = event.recipient.id;
  var timeOfPostback = event.timestamp;

  // Get the sessionId for the give sender
  const sessionId = findOrCreateSession(senderId);

  // The 'payload' param is a developer-defined field which is set in a postback 
  // button for Structured Messages. 
  var payload = event.postback.payload;

  console.log("Received postback for user %d and page %d with payload '%s' " + 
    "at %d", senderId, recipientId, payload, timeOfPostback);

  // Process the postback through Wit
  processTextMessage(sessionId, payload);
};

/*
 * Send a text message using the Send API.
 *
 */
const sendTextMessage = (recipientId, messageText) => {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: messageText
    }
  };

  callSendAPI(messageData, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      var recipientId = body.recipient_id;
      var messageId = body.message_id;

      console.log("Successfully sent test message with id %s to recipient %s", messageId, recipientId);
    } else {
      console.error("Unable to send message.");
      console.error(response);
      console.error(error);
    }
  });
};

const sendTemplateMessage = (recipientId, messagePayload, callback) => {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: messagePayload
      }
    }
  }; 

  callSendAPI(messageData, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      var recipientId = body.recipient_id;
      var messageId = body.message_id;

      console.log("Successfully sent templated message with id %s to recipient %s", messageId, recipientId);
    } else {
      console.error("Unable to send message.");
      console.error(response);
      console.error(error);
    }

    // Let's give the wheel back to our bot
    if(callback) {
      callback();
    }
  });
};

/*
 * Call the Send API. The message data goes in the body. If successful, we'll 
 * get the message id in a response 
 *
 */




const callSendAPI = (messageData, callback) => {
  request({
    uri: 'https://graph.facebook.com/v2.6/me/messages',
    qs: { access_token: PAGE_ACCESS_TOKEN },
    method: 'POST',
    json: messageData
  }, (error, response, body) => {
    if (callback) {
      callback(error, response, body);
    }
  });  
};

// Setting up our Express App

const app = express();

app.set('port', process.env.PORT || 5000);
app.use(bodyParser.json({ verify: verifyRequestSignature }));
app.use(express.static('public'));

/*
 * Use your own validation token. Check that the token used in the Webhook 
 * setup is the same token used here.
 */
app.get('/webhook', function(req, res) {
  if (req.query['hub.mode'] === 'subscribe' &&
      req.query['hub.verify_token'] === VALIDATION_TOKEN) {
    console.log("Validating webhook");
    res.status(200).send(req.query['hub.challenge']);
  } else {
    console.error("Failed validation. Make sure the validation tokens match.");
    res.sendStatus(403);          
  }  
});

/*
 * All callbacks for Messenger are POST-ed. They will be sent to the same
 * webhook. Be sure to subscribe your app to your page to receive callbacks
 * for your page. 
 * https://developers.facebook.com/docs/messenger-platform/implementation#subscribe_app_pages
 */
app.post('/webhook', function (req, res) {

  var data = req.body;

  // Make sure this is a page subscription
  if (data.object == 'page') {
    // Iterate over each entry
    // There may be multiple if batched
    data.entry.forEach(function(pageEntry) {
      var pageId = pageEntry.id;
      var timeOfEvent = pageEntry.time;

      // Iterate over each messaging event
      pageEntry.messaging.forEach(function(messagingEvent) {
        if (messagingEvent.optin) {
          receivedAuthentication(messagingEvent);
        } else if (messagingEvent.message) {
          receivedMessage(messagingEvent);
        } else if (messagingEvent.delivery) {
          receivedDeliveryConfirmation(messagingEvent);
        } else if (messagingEvent.postback) {
          receivedPostback(messagingEvent);
        } else {
          console.log("Webhook received unknown messagingEvent: ", messagingEvent);
        }
      });
    });

    // You must send back a 200, within 20 seconds, to let us know you've 
    // successfully received the callback. Otherwise, the request will time out.
    res.sendStatus(200);
  }
});

// Start server
// Webhooks must be available via SSL with a certificate signed by a valid 
// certificate authority.
app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});

module.exports = app;