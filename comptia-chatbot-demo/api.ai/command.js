#!/usr/bin/env node
/*
 * Interactive command line application to test
 * the Api.Ai conversation features.
 * To start type:
 * 
 * node ./command.js
 * 
 * To exit hit Ctrl + C or type 'exit' or 'quit'
 */

const readline = require('readline'),
	config = require('config'),
  apiai = require('apiai');

// Api.Ai access token
const APIAI_TOKEN = (process.env.APIAI_TOKEN) ? 
  (process.env.APIAI_TOKEN) : 
  config.get('ApiAi.token');

if (!APIAI_TOKEN) {
  console.error("Missing api config token");
  process.exit(1);
}

const api = apiai(APIAI_TOKEN);
let is_debug = false;

// Start the interactive console
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.on('line', (input) => {
	if(input === 'quit' || input === 'exit') {
		// Exit the interactive console
		process.exit(0);
	} else if(input === 'debug on') {
		// Turn on debug mode. Prints the full response object to the console
		is_debug = true;
		console.log('Debug mode On');
	} else if(input === 'debug off') {
		// Turn off debug mode. Only print the response text to the console
		is_debug = false;
		console.log('Debug mode Off');
	} else {
		const parameters = {
			sessionId : '6d7c5d60-1b9a-4d53-a5e0-f8607f79acce' 
		};

		// Send the text input to Api.Ai for processing 
		const request = api.textRequest(input, parameters);

		request.on('response', function(response) {
			if(is_debug) {
				console.log(response);
			} else {
				if(response.result && response.result.fulfillment
					&& response.result.fulfillment.speech) {
					const message = response.result.fulfillment.speech;
					console.log('> ' + message);
				}
			}
		});

		request.on('error', function(error) {
				console.log(error);
		});

		request.end();
	}
});

console.log('Hit Ctrl + C or type \'quit\' to exit');