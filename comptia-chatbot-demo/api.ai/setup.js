/* jshint esversion: 6, node: true, devel: true */
'use strict';

const config = require('config'),
  request = require('request');

const PAGE_ACCESS_TOKEN = (process.env.MESSENGER_PAGE_ACCESS_TOKEN) ?
  (process.env.MESSENGER_PAGE_ACCESS_TOKEN) :
  config.get('Facebook.pageAccessToken');

if (!(PAGE_ACCESS_TOKEN)) {
  console.error("Missing config values");
  process.exit(1);
}

// Setup the Welcome Message
/*request({
    uri: 'https://graph.facebook.com/v2.6/me/thread_settings?access_token=' + PAGE_ACCESS_TOKEN,
    method: 'POST',
    json: {
      "setting_type":"greeting",
      "greeting":{
        "text":"Hi! I’m here to help you find the right insurance plan."
      }
    }
  }, (error, response, body) => {
    if (!error) {
      console.log(body.result);
    } else {
      console.log(error);
    }
  }
);*/

// Setup the Call to Actions Button
request({
    uri: 'https://graph.facebook.com/v2.6/me/thread_settings?access_token=' + PAGE_ACCESS_TOKEN,
    method: 'POST',
    json: {
      "setting_type":"call_to_actions",
      "thread_state":"new_thread",
      "call_to_actions":[
        {
          "payload":"Hi"
        }
      ]
    }
  }, (error, response, body) => {
    if (!error) {
      console.log(body.result);
    } else {
      console.log(error);
    }
  }
);

// Setup the Persistent Menu
request({
    uri: 'https://graph.facebook.com/v2.6/me/thread_settings?access_token=EAAIriLbZBKjsBANyjMK1y2yOHK78LUJChUGE3YVrWP1LUH3ffaoLHOgZCTUWtY0aGdAcmZArZCKWhS4kofxO5KhOgAebPcx0sgNPZBcKCzXmpMuh6VSFmb1VlPqjPJK58A9ZA8XY692J9pBk6mbeARKWZAZCcNL0SXaqaRu7PZBVCuQZDZD', //+ PAGE_ACCESS_TOKEN,
    method: 'POST',
    json: {
      "setting_type" : "call_to_actions",
      "thread_state" : "existing_thread",
      "call_to_actions":[
        {
          "type":"postback",
          "title":"Something is wrong",
          "payload":"Something is wrong"
        },
        {
          "type":"postback",
          "title":"General Questions",
          "payload":"Something else"
        },
        {
          "type":"postback",
          "title":"I love the app!",
          "payload":"Everything is awesome and amazing"
        }
      ]
    }
  }, (error, response, body) => {
    if (!error) {
      console.log(body.result);
    } else {
      console.log(error);
    }
  }
);
