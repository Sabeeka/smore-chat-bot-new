/*
 * Copyright 2016-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

/* jshint esversion: 6, node: true, devel: true */
'use strict';

const 
  bodyParser = require('body-parser'),
  config = require('config'),
  crypto = require('crypto'),
  express = require('express'),
  https = require('https'),  
  request = require('request'),
  Logger = require('node-wit').Logger,
  levels = require('node-wit').logLevels,
  Wit = require('node-wit').Wit,
  messages = require("./messages");

/*
 * Be sure to setup your config values before running this code. You can 
 * set them using environment variables or modifying the config file in /config.
 */

// App Secret can be retrieved from the App Dashboard
const APP_SECRET = (process.env.MESSENGER_APP_SECRET) ? 
  process.env.MESSENGER_APP_SECRET :
  config.get('Facebook.appSecret');

// Arbitrary value used to validate a webhook
const VALIDATION_TOKEN = (process.env.MESSENGER_VALIDATION_TOKEN) ?
  (process.env.MESSENGER_VALIDATION_TOKEN) :
  config.get('Facebook.validationToken');

// Generate a page access token for your page from the App Dashboard
const PAGE_ACCESS_TOKEN = (process.env.MESSENGER_PAGE_ACCESS_TOKEN) ?
  (process.env.MESSENGER_PAGE_ACCESS_TOKEN) :
  config.get('Facebook.pageAccessToken');

// Wit.ai access token
const WIT_TOKEN = (process.env.WIT_TOKEN) ? 
  (process.env.WIT_TOKEN) : 
  config.get('Wit.token');

if (!(APP_SECRET && VALIDATION_TOKEN && PAGE_ACCESS_TOKEN && WIT_TOKEN)) {
  console.error("Missing config values");
  process.exit(1);
}

const action_welcome = (sessionId, context, callback) => {
  // Let's retrieve the Facebook user whose session belongs to
  const recipientId = sessions[sessionId].fbid;
  // Send structured message view Messanger Api

  if (recipientId) {
    // Yay, we found our recipient!
    // Let's forward our bot response to her.

    sendTextMessage(recipientId, messages.RESPONSE_WELCOME);

    sendTemplateMessage(recipientId, {
        template_type: "button",
        text: messages.RESPONSE_HELP,
        buttons:[{
          type: "postback",
          title: messages.BUTTON_INTERNET_CONNECTIVITY,
          payload: messages.ISSUE_INTERNET_CONNECTIVITY
        }, {
          type: "postback",
          title: messages.BUTTON_SLOW_COMPUTER,
          payload: messages.ISSUE_SLOW_COMPUTER
        }, {
          type: "postback",
          title: messages.BUTTON_PURCHASING_ADVICE,
          payload: messages.ISSUE_PURCHASING_ADVICE
        }]
      }, callback
    );
  } else {
    console.log('Oops! Couldn\'t find user for session:', sessionId);
    // Giving the wheel back to our bot
    callback(context);
  }
};

const action_help = (sessionId, context, callback) => {
  // Let's retrieve the Facebook user whose session belongs to
  const recipientId = sessions[sessionId].fbid;

  if (recipientId) {
    //Send structured message view Messanger Api
    sendTextMessage(recipientId, messages.RESPONSE_HELP, callback);
  }
}

const action_purchasing_advice = (sessionId, context, callback) => {
  // Let's retrieve the Facebook user whose session belongs to
  const recipientId = sessions[sessionId].fbid;

  if (recipientId) {
    //Send structured message view Messanger Api
    sendTextMessage(recipientId, messages.RESPONSE_PURCHASING_ADVICE, callback);
  } else {
    console.log('Oops! Couldn\'t find user for session:', sessionId);
    // Giving the wheel back to our bot
    callback(context);
  }
}

const action_slow_computer = (sessionId, context, callback) => {
  // Let's retrieve the Facebook user whose session belongs to
  const recipientId = sessions[sessionId].fbid;
  
  if (recipientId) {
    //Send structured message view Messanger Api
    sendTextMessage(recipientId, messages.RESPONSE_SLOW_COMPUTER, callback);
  } else {
    console.log('Oops! Couldn\'t find user for session:', sessionId);
    // Giving the wheel back to our bot
    callback(context);
  }
}

const action_internet_connectivity = (sessionId, context, callback) => {
  // Let's retrieve the Facebook user whose session belongs to
  const recipientId = sessions[sessionId].fbid;
  
  if (recipientId) {
    //Send structured message view Messanger Api
    sendTextMessage(recipientId, messages.RESPONSE_INTERNET_CONNECTIVITY, callback);
  } else {
    console.log('Oops! Couldn\'t find user for session:', sessionId);
    // Giving the wheel back to our bot
    callback(context);
  }
}

// Wit.ai bot specific code

// This will contain all user sessions.
// Each session has an entry:
// sessionId -> {fbid: facebookUserId, context: sessionState}
const sessions = {};

const findOrCreateSession = (fbid) => {
  let sessionId;
  // Let's see if we already have a session for the user fbid
  Object.keys(sessions).forEach(k => {
    if (sessions[k].fbid === fbid) {
      // Yep, got it!
      sessionId = k;
    }
  });
  if (!sessionId) {
    // No session found for user fbid, let's create a new one
    sessionId = new Date().toISOString();
    sessions[sessionId] = {fbid: fbid, context: {}};
  }
  return sessionId;
};

// Find the first value matching the entity
const firstEntityValue = (entities, entity) => {
  const val = entities && entities[entity] &&
    Array.isArray(entities[entity]) &&
    entities[entity].length > 0 &&
    entities[entity][0].value
  ;
  if (!val) {
    return null;
  }
  return typeof val === 'object' ? val.value : val;
};

// Our bot actions
const actions = {
  say(sessionId, context, message, callback) {
    // Our bot has something to say!
    // Let's retrieve the Facebook user whose session belongs to
    const recipientId = sessions[sessionId].fbid;
    if (recipientId) {
      // Yay, we found our recipient!
      // Let's forward our bot response to her.
      sendTextMessage(recipientId, message, callback);
    } else {
      console.log('Oops! Couldn\'t find user for session:', sessionId);
      // Giving the wheel back to our bot
      callback(context);
    }
  },
  merge(sessionId, context, entities, message, callback) {
    const intent = firstEntityValue(entities, 'intent');
    if (intent) {
      context.intent = intent;
    }
    const device = firstEntityValue(entities, 'device');
    if (device) {
      context.device = device;
    }
    callback(context);
  },
  error(sessionId, context, error) {
    console.log(error.message);
  },
  ['welcome'] : action_welcome,
  ['help'] : action_help,
  ['issue-purchasing-advice'] : action_purchasing_advice,
  ['issue-slow-computer'] : action_slow_computer,
  ['issue-internet-connectivity'] : action_internet_connectivity,
  // You should implement your custom actions here
  // See https://wit.ai/docs/quickstart
};

// Setting up our bot
const logger = new Logger(levels.DEBUG);
const wit = new Wit(WIT_TOKEN, actions, logger);

/*
 * Verify that the callback came from Facebook. Using the App Secret from 
 * the App Dashboard, we can verify the signature that is sent with each 
 * callback in the x-hub-signature field, located in the header.
 *
 * https://developers.facebook.com/docs/graph-api/webhooks#setup
 *
 */
const verifyRequestSignature = (req, res, buf) => {
  var signature = req.headers["x-hub-signature"];

  if (!signature) {
    // For testing, let's log an error. In production, you should throw an 
    // error.
    console.error("Couldn't validate the signature.");
  } else {
    var elements = signature.split('=');
    var method = elements[0];
    var signatureHash = elements[1];

    var expectedHash = crypto.createHmac('sha1', APP_SECRET).update(buf).digest('hex');

    if (signatureHash != expectedHash) {
      throw new Error("Couldn't validate the request signature.");
    }
  }
};

/*
 * Authorization Event
 *
 * The value for 'optin.ref' is defined in the entry point. For the "Send to 
 * Messenger" plugin, it is the 'data-ref' field. Read more at 
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference#auth
 *
 */
const receivedAuthentication = (event) => {
  var senderId = event.sender.id;
  var recipientId = event.recipient.id;
  var timeOfAuth = event.timestamp;

  // The 'ref' field is set in the 'Send to Messenger' plugin, in the 'data-ref'
  // The developer can set this to an arbitrary value to associate the 
  // authentication callback with the 'Send to Messenger' click event. This is
  // a way to do account linking when the user clicks the 'Send to Messenger' 
  // plugin.
  var passThroughParam = event.optin.ref;

  console.log("Received authentication for user %d and page %d with pass " +
    "through param '%s' at %d", senderId, recipientId, passThroughParam, 
    timeOfAuth);

  // When an authentication is received, we'll send a message back to the sender
  // to let them know it was successful.
  sendTextMessage(senderId, "Authentication successful");
};

/*
 * Message Event
 *
 * This event is called when a message is sent to your page. The 'message' 
 * object format can vary depending on the kind of message that was received.
 * Read more at https://developers.facebook.com/docs/messenger-platform/webhook-reference#received_message
 *
 * For this example, we're going to echo any text that we get. If we get some 
 * special keywords ('button', 'generic', 'receipt'), then we'll send back
 * examples of those bubbles to illustrate the special message bubbles we've 
 * created. If we receive a message with an attachment (image, video, audio), 
 * then we'll simply confirm that we've received the attachment.
 * 
 */
const receivedMessage = (event) => {
  var senderId = event.sender.id;
  var recipientId = event.recipient.id;
  var timeOfMessage = event.timestamp;
  var message = event.message;

  console.log("Received message for user %d and page %d at %d with message:", senderId, recipientId, timeOfMessage);
  console.log(JSON.stringify(message));

  var messageId = message.mid;

  // You may get a text or attachment but not both
  var messageText = message.text;
  var messageAttachments = message.attachments;

  if (messageText) {

    // We retrieve the user's current session, or create one if it doesn't exist
    // This is needed for our bot to figure out the conversation history
    const sessionId = findOrCreateSession(senderId);

    // Process the message text
    processTextMessage(sessionId, messageText);
  } else if (messageAttachments) {
    sendTextMessage(senderId, "Sorry but we currently cannot process messages with attachements.");
  }
};

/*
 * Wit Message Processing Event
 *
 * Send the message to Wit for processing of the user's intent
 *
 */
const processTextMessage = (sessionId, messageText) => {
  // Let's forward the message to the Wit.ai Bot Engine
  // This will run all actions until our bot has nothing left to do
  wit.runActions(
    sessionId, // the user's current session
    messageText, // the user's message 
    sessions[sessionId].context, // the user's current session state
    (error, context) => {
      if (error) {
        console.log('Oops! Got an error from Wit:', error);
      } else {
        // Our bot did everything it has to do.
        // Now it's waiting for further messages to proceed.
        console.log('Waiting for futher messages.');

        // Based on the session state, you might want to reset the session.
        // This depends heavily on the business logic of your bot.
        // Example:
        // if (context['done']) {
        //   delete sessions[sessionId];
        // }

        // Updating the user's current session state
        sessions[sessionId].context = context;
      }
    }
  );
};

/*
 * Delivery Confirmation Event
 *
 * This event is sent to confirm the delivery of a message. Read more about 
 * these fields at https://developers.facebook.com/docs/messenger-platform/webhook-reference#message_delivery
 *
 */
const receivedDeliveryConfirmation = (event) => {
  var senderId = event.sender.id;
  var recipientId = event.recipient.id;
  var delivery = event.delivery;
  var messageIDs = delivery.mids;
  var watermark = delivery.watermark;
  var sequenceNumber = delivery.seq;

  if (messageIDs) {
    messageIDs.forEach(function(messageID) {
      console.log("Received delivery confirmation for message ID: %s", messageID);
    });
  }

  console.log("All message before %d were delivered.", watermark);
};

/*
 * Postback Event
 *
 * This event is called when a postback is tapped on a Structured Message. Read
 * more at https://developers.facebook.com/docs/messenger-platform/webhook-reference#postback
 * 
 */
const receivedPostback = (event) => {
  var senderId = event.sender.id;
  var recipientId = event.recipient.id;
  var timeOfPostback = event.timestamp;

  // Get the sessionId for the give sender
  const sessionId = findOrCreateSession(senderId);

  // The 'payload' param is a developer-defined field which is set in a postback 
  // button for Structured Messages. 
  var payload = event.postback.payload;

  console.log("Received postback for user %d and page %d with payload '%s' " + 
    "at %d", senderId, recipientId, payload, timeOfPostback);

  // Process the postback through Wit
  processTextMessage(sessionId, payload);
};

/*
 * Send a message with an using the Send API.
 *
 */
const sendImageMessage = (recipientId) => {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "image",
        payload: {
          url: "http://i.imgur.com/zYIlgBl.png"
        }
      }
    }
  };

  callSendAPI(messageData);
};

/*
 * Send a text message using the Send API.
 *
 */
const sendTextMessage = (recipientId, messageText, callback) => {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: messageText
    }
  };

  callSendAPI(messageData, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      var recipientId = body.recipient_id;
      var messageId = body.message_id;

      console.log("Successfully sent test message with id %s to recipient %s", messageId, recipientId);
    } else {
      console.error("Unable to send message.");
      console.error(response);
      console.error(error);
    }

    // Let's give the wheel back to our bot
    if(callback) {
      callback();
    }
  });
};

const sendTemplateMessage = (recipientId, messagePayload, callback) => {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: messagePayload
      }
    }
  }; 

  callSendAPI(messageData, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      var recipientId = body.recipient_id;
      var messageId = body.message_id;

      console.log("Successfully sent templated message with id %s to recipient %s", messageId, recipientId);
    } else {
      console.error("Unable to send message.");
      console.error(response);
      console.error(error);
    }

    // Let's give the wheel back to our bot
    if(callback) {
      callback();
    }
  });
};

/*
 * Send a button message using the Send API.
 *
 */
const sendButtonMessage = (recipientId) => {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "button",
          text: "This is test text",
          buttons:[{
            type: "web_url",
            url: "https://www.oculus.com/en-us/rift/",
            title: "Open Web URL"
          }, {
            type: "postback",
            title: "Call Postback",
            payload: "Developer defined postback"
          }]
        }
      }
    }
  };  

  callSendAPI(messageData);
};

/*
 * Send a Structured Message (Generic Message type) using the Send API.
 *
 */
const sendGenericMessage = (recipientId, messageTemplate, callback) => {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {          
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [{
            title: "rift",
            subtitle: "Next-generation virtual reality",
            item_url: "https://www.oculus.com/en-us/rift/",               
            image_url: "http://messengerdemo.parseapp.com/img/rift.png",
            buttons: [{
              type: "web_url",
              url: "https://www.oculus.com/en-us/rift/",
              title: "Open Web URL"
            }, {
              type: "postback",
              title: "Call Postback",
              payload: "Payload for first bubble",
            }],
          }, {
            title: "touch",
            subtitle: "Your Hands, Now in VR",
            item_url: "https://www.oculus.com/en-us/touch/",               
            image_url: "http://messengerdemo.parseapp.com/img/touch.png",
            buttons: [{
              type: "web_url",
              url: "https://www.oculus.com/en-us/touch/",
              title: "Open Web URL"
            }, {
              type: "postback",
              title: "Call Postback",
              payload: "Payload for second bubble",
            }]
          }]
        }
      }
    }
  };  

  callSendAPI(messageData);
};

/*
 * Send a receipt message using the Send API.
 *
 */
const sendReceiptMessage = (recipientId) => {
  // Generate a random receipt ID as the API requires a unique ID
  var receiptId = "order" + Math.floor(Math.random()*1000);

  var messageData = {
    recipient: {
      id: recipientId
    },
    message:{
      attachment: {
        type: "template",
        payload: {
          template_type: "receipt",
          recipient_name: "Peter Chang",
          order_number: receiptId,
          currency: "USD",
          payment_method: "Visa 1234",        
          timestamp: "1428444852", 
          elements: [{
            title: "Oculus Rift",
            subtitle: "Includes: headset, sensor, remote",
            quantity: 1,
            price: 599.00,
            currency: "USD",
            image_url: "http://messengerdemo.parseapp.com/img/riftsq.png"
          }, {
            title: "Samsung Gear VR",
            subtitle: "Frost White",
            quantity: 1,
            price: 99.99,
            currency: "USD",
            image_url: "http://messengerdemo.parseapp.com/img/gearvrsq.png"
          }],
          address: {
            street_1: "1 Hacker Way",
            street_2: "",
            city: "Menlo Park",
            postal_code: "94025",
            state: "CA",
            country: "US"
          },
          summary: {
            subtotal: 698.99,
            shipping_cost: 20.00,
            total_tax: 57.67,
            total_cost: 626.66
          },
          adjustments: [{
            name: "New Customer Discount",
            amount: -50
          }, {
            name: "$100 Off Coupon",
            amount: -100
          }]
        }
      }
    }
  };

  callSendAPI(messageData);
};

/*
 * Call the Send API. The message data goes in the body. If successful, we'll 
 * get the message id in a response 
 *
 */
const callSendAPI = (messageData, callback) => {
  request({
    uri: 'https://graph.facebook.com/v2.6/me/messages',
    qs: { access_token: PAGE_ACCESS_TOKEN },
    method: 'POST',
    json: messageData
  }, (error, response, body) => {
    if (callback) {
      callback(error, response, body);
    }
  });  
};

// Setting up our Express App

const app = express();

app.set('port', process.env.PORT || 5000);
app.use(bodyParser.json({ verify: verifyRequestSignature }));
app.use(express.static('public'));

/*
 * Use your own validation token. Check that the token used in the Webhook 
 * setup is the same token used here.
 */
app.get('/webhook', function(req, res) {
  if (req.query['hub.mode'] === 'subscribe' &&
      req.query['hub.verify_token'] === VALIDATION_TOKEN) {
    console.log("Validating webhook");
    res.status(200).send(req.query['hub.challenge']);
  } else {
    console.error("Failed validation. Make sure the validation tokens match.");
    res.sendStatus(403);          
  }  
});

/*
 * All callbacks for Messenger are POST-ed. They will be sent to the same
 * webhook. Be sure to subscribe your app to your page to receive callbacks
 * for your page. 
 * https://developers.facebook.com/docs/messenger-platform/implementation#subscribe_app_pages
 */
app.post('/webhook', function (req, res) {

  var data = req.body;

  // Make sure this is a page subscription
  if (data.object == 'page') {
    // Iterate over each entry
    // There may be multiple if batched
    data.entry.forEach(function(pageEntry) {
      var pageId = pageEntry.id;
      var timeOfEvent = pageEntry.time;

      // Iterate over each messaging event
      pageEntry.messaging.forEach(function(messagingEvent) {
        if (messagingEvent.optin) {
          receivedAuthentication(messagingEvent);
        } else if (messagingEvent.message) {
          receivedMessage(messagingEvent);
        } else if (messagingEvent.delivery) {
          receivedDeliveryConfirmation(messagingEvent);
        } else if (messagingEvent.postback) {
          receivedPostback(messagingEvent);
        } else {
          console.log("Webhook received unknown messagingEvent: ", messagingEvent);
        }
      });
    });

    // You must send back a 200, within 20 seconds, to let us know you've 
    // successfully received the callback. Otherwise, the request will time out.
    res.sendStatus(200);
  }
});

// Start server
// Webhooks must be available via SSL with a certificate signed by a valid 
// certificate authority.
app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});

module.exports = app;