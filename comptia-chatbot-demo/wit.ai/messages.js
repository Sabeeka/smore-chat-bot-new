var define = require("node-constants")(exports);
// define is a function that binds "constants" to an object (commonly exports)

// or multiple
define({
	RESPONSE_WELCOME: 'Hi! I’m Rocco. I’m here to collect information that will allow our CompTIA IT professional help you today.',
	RESPONSE_HELP: 'What would you like help with?',
	BUTTON_INTERNET_CONNECTIVITY: 'Internet Connection',
	BUTTON_SLOW_COMPUTER: 'Slow Computer',
	BUTTON_PURCHASING_ADVICE: 'Purchasing Advice',
	ISSUE_INTERNET_CONNECTIVITY: 'I\'m having trouble with my internet connection',
	ISSUE_SLOW_COMPUTER: 'I have a slow computer',
	ISSUE_PURCHASING_ADVICE: 'I need purchasing advice',
	RESPONSE_SLOW_COMPUTER: 'Ok, got it, your computer is running slow. Can you briefly describe which applications or programs are running slowly?',
	RESPONSE_INTERNET_CONNECTIVITY: 'Ok, got it, you’re having internet issues. And what type of device/devices are you primarily trying to connect? Is it a computer, tablet, or smartphone? ',
	RESPONSE_PURCHASING_ADVICE: 'Ok, got it, you’d like some help purchasing a device. Which type of device are you looking to buy?',
});